#include <msp430g2553.h>

int flag = 0; /*0=arret; 1=avancer; -1=reculer*/

void init_bouton(void)
{
    //bouton demarrage
    P1DIR &=~ BIT3;
    P1REN |= BIT3;
    P1OUT |= BIT3;
    P1IE |= BIT3;
    P1IES |= BIT3;
    P1IFG &=~ BIT3;
}

void init_moteur(void){

    P2SEL |= (BIT2 | BIT4);
    P2SEL2 &=~ (BIT2 | BIT4);

    P2DIR |= (BIT2 | BIT4 | BIT1 | BIT5);

    TA1CTL |= (TASSEL_2 | MC_1 | ID_0);  //Problem is here

    TA1CCTL1 |= OUTMOD_7;
    TA1CCTL2 |= OUTMOD_7;

    P2OUT |= BIT5; //sens moteur B
    P2OUT &=~ BIT1; //sens moteur A

    TA1CCR0 = 100;
}

void arreter()
{
    P2OUT &=~ (BIT5 | BIT1);
    TA1CCR1 = 0; //moteur gauche
    TA1CCR2 = 0; //moteur droit
    flag = 0;
}

void avancer()
{
    P2OUT &=~ BIT1;
    P2OUT |= BIT5;
    TA1CCR1 = 50; //moteur gauche
    TA1CCR2 = 50; //moteur droit
    flag = 1;
}


void reculer()
{
    P2OUT |= BIT1;
    P2OUT &=~ BIT5;
    TA1CCR1 = 50; //moteur gauche
    TA1CCR2 = 50; //moteur droit
    flag = -1;
}


void tourner_gauche()
{
    P2OUT &=~ BIT1;
    P2OUT |= BIT5;
    TA1CCR1 = 0; //moteur gauche
    TA1CCR2 = 50; //moteur droit
    __delay_cycles(500000);

    if (flag == 1)
    {
        avancer();
    }
    else if (flag == -1)
    {
        reculer();
    }
    else
    {
        arreter();
    }

}


void tourner_droite()
{
    TA1CCR1 = 50; //moteur gauche
    TA1CCR2 = 0; //moteur droit
    P2OUT &=~ BIT1;
    P2OUT |= BIT5;
    __delay_cycles(500000);

    if (flag == 1)
    {
        avancer();
    }
    else if (flag == -1)
    {
        reculer();
    }
    else
    {
        arreter();
    }
}

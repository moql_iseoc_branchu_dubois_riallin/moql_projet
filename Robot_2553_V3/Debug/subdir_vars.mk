################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430g2553.cmd 

C_SRCS += \
../SPI_2553.c \
../UART_2553.c \
../main_robot.c \
../robot_sambot.c 

C_DEPS += \
./SPI_2553.d \
./UART_2553.d \
./main_robot.d \
./robot_sambot.d 

OBJS += \
./SPI_2553.obj \
./UART_2553.obj \
./main_robot.obj \
./robot_sambot.obj 

OBJS__QUOTED += \
"SPI_2553.obj" \
"UART_2553.obj" \
"main_robot.obj" \
"robot_sambot.obj" 

C_DEPS__QUOTED += \
"SPI_2553.d" \
"UART_2553.d" \
"main_robot.d" \
"robot_sambot.d" 

C_SRCS__QUOTED += \
"../SPI_2553.c" \
"../UART_2553.c" \
"../main_robot.c" \
"../robot_sambot.c" 



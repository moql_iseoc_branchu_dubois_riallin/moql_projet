#include <msp430g2553.h>
#include <string.h>

#include "robot_sambot.h"
#include "UART_2553.h"
#include "SPI_2553.h"


/* Initialisation de la carte*/
void init_board(void)
{
    WDTCTL = WDTPW + WDTHOLD; /*Stop WDT*/
    /*clock calibration verification*/

    if(CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
      __low_power_mode_4();

    BCSCTL1 = CALBC1_1MHZ; /*Set DCO*/
    DCOCTL = CALDCO_1MHZ;
}

void main(void)
{

    init_board();
    init_SPI();
    init_UART();
    init_moteur();

    __bis_SR_register(LPM0_bits + GIE); /*Enter LPM0, interrupts enabled*/

    while(1){}

}


#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    unsigned char carac;
    unsigned char consigne;

    /* UART */
    if (IFG2 & UCA0RXIFG)
    {
        carac= UCA0RXBUF;
        menu_UART(carac);
    }

    /* SPI */
    else if (IFG2 & UCB0RXIFG)
    {
        while( (UCB0STAT & UCBUSY) && !(UCB0STAT & UCOE) );
        while(!(IFG2 & UCB0RXIFG));
        consigne = UCB0RXBUF;
        menu_SPI(consigne);
    }
}


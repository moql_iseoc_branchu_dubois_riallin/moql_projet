#include <msp430.h> 
#include "SPI_2231.h"


/**
 * main.c
 */
int main(void)
{
	init_board();
	init_SPI();

    __bis_SR_register(LPM4_bits | GIE); // general interrupts enable & Low Power Mode
	
	return 0;
}

//interruption � modifier pour faire de la transmission
#pragma vector=USI_VECTOR
__interrupt void universal_serial_interface(void)
{

    volatile unsigned char RXDta;

    while( !(USICTL1 & USIIFG) );   // waiting char by USI counter flag
    RXDta = USISRL;

    if (RXDta == 0x31) //if the input buffer is 0x31 (mainly to read the buffer)
    {
        P1OUT |= BIT0; //turn on LED
    }
    else if (RXDta == 0x30)
    {
        P1OUT &= ~BIT0; //turn off LED
    }
    USISRL = RXDta;
    USICNT &= ~USI16B;  // re-load counter & ignore USISRH
    USICNT = 0x08;      // 8 bits count, that re-enable USI for next transfert
}

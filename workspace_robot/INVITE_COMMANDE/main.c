/**
 * Communication UART entre le BT RN -42 et le 2553
 */


#include <msp430.h> 
#include <string.h>



/**
 * Initialisation des entr�es-sorties : leds disponibles mais �teintes
 * R�sistance de tirage sur P1.6
 */
void InitIO(void)
{
    P1DIR |= (BIT0 & BIT6);
    P1OUT &=~ (BIT0 & BIT6);
    P1REN |= BIT6;

    P2DIR |= (BIT1 | BIT2 | BIT4 | BIT5);
    P2OUT &=~ (BIT1 | BIT2 | BIT4 | BIT5);
}

/**
 * Initialisation des ports de l'UART
 */
void InitUART(void)
{
    /*Les ports de l'UART sur le 2553 sont P1.1 et P1.2*/
    P1SEL |= (BIT1 & BIT2);
    P1SEL2 |= (BIT1 & BIT2);

    /*Selection SMCLK*/
    UCA0CTL1 |= (BIT7 & BIT6); //UCSSEL0 = 0b11000000 : cf datasheet p.340

    /*On a F=1MHz et BaudRate=9600 : d'apr�s la datasheet p.424*/
    UCA0BR0 = 104;
    UCA0BR1 = 0;

    /**
     * Configuration du registre UCA0CTL0 : datasheet p.429
     * BIT1 et BIT2 correspondent � UCMODE0 = 0b00*/
    UCA0CTL0 &=~ (UCPEN & UCMSB & UC7BIT & UCSPB & BIT2 & BIT1 & UCSYNC);

    /**
     * Fin de configuration du registre UCA0CTL1 : datasheet p.430
     */
    UCA0CTL1 &=~ UCSWRST;
}

/*
 * Reception d'un caract�re re�u
 */
void RxData(unsigned char *c)
{
    while(!(IFG2 & UCA0RXIFG));     //Attente lib�ration du buffer
    *c = UCA0RXBUF;
}

/*
 * Emission d'un caract�re
 */
void TxData(unsigned char *c)
{
    while(!(IFG2 & UCA0TXIFG));     //Attente lib�ration du buffer
    UCA0TXBUF = c;
}

/*
 * Envoi d'un message
 */
void Send_STR_UART(const char *msg)
{
    int i=0;
    for (i = 0 ; msg[i] != 0x00 ; i++)
    {
        TxData(msg[i]);
    }
}


/*
 * Fonction de l'invite de commande
 */
void command(char *cmd)
{
    if(strcmp(cmd, "h") == 0)          // aide
    {
        P1OUT |= BIT6;
        Send_STR_UART("\r\nAide : commandes existantes :\n");
        Send_STR_UART("\r\n'ver' : version\n");
        Send_STR_UART("\r\n'z' : avancer\n");
        Send_STR_UART("\r\n's' : reculer\n");
        Send_STR_UART("\r\n'q' : gauche\n");
        Send_STR_UART("\r\n'd' : droite\n");
        Send_STR_UART("\r\n'h' : affichage de cette aide\n");
    }

    else if (strcmp(cmd, "ver") == 0)     // version
    {
        Send_STR_UART("\rRobot Pilote v1.0\n");
    }

    else if(strcmp(cmd, "z") == 0)     // allumage led rouge
    {
        P2OUT |= BIT2 | BIT4;
        P2OUT &=~ (BIT1 | BIT5);
        Send_STR_UART("\rAvancer\n");
    }

    else if(strcmp(cmd, "s") == 0)     // allumage led rouge
    {
        P2OUT |= BIT1 | BIT2 | BIT4 | BIT5;
        Send_STR_UART("\rReculer\n");
    }

    else if(strcmp(cmd, "q") == 0)     // allumage led rouge
    {
        P2OUT |= BIT2;
        P2OUT &=~ (BIT1 | BIT4 | BIT5);
        Send_STR_UART("\rGauche\n");
    }

    else if(strcmp(cmd, "d") == 0)     // allumage led rouge
    {
        P2OUT |= BIT4;
        P2OUT &=~ (BIT1 | BIT2 | BIT5);
        Send_STR_UART("\rDroite\n");
    }

    else
    {
        Send_STR_UART("\rMauvaise commande ");
        Send_STR_UART(cmd);
        Send_STR_UART("\rEntrez 'h' pour l'aide\n");
    }
}



int main(void)
{
    unsigned char c;
  char  cmd[12];      // tableau de caractere lie a la commande user
  int   nb_car;           // compteur nombre carateres saisis

    WDTCTL = WDTPW + WDTHOLD;   // Stop WDT
    // clock calibration verification
    if(CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
      __low_power_mode_4();
    // factory calibration parameters
    DCOCTL = 0;
    BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

    InitIO();
    InitUART();

    nb_car = 0;
    Send_STR_UART("MSP430 Ready !");
    while(1)
    {
      if( nb_car<(12-1) )
      {
        RxData(&c);
        if( (cmd[nb_car]=c) != '\n' )
        {
            TxData(c);
            nb_car++;
        }
        else
        {
            cmd[nb_car]=0x00;
            command(cmd);
            nb_car=0;
        }
      }
    }
}

#include <msp430g2553.h>
#include <string.h>

#include "robot_sambot.h"


void init_UART(void)
{
    P1SEL |= (BIT1 | BIT2);                 /*P1.1 = RXD, P1.2=TXD*/
    P1SEL2 |= (BIT1 | BIT2);                /*P1.1 = RXD, P1.2=TXD*/

    UCA0CTL1 |= UCSSEL_2;                   /*SMCLK*/
    UCA0CTL1 &= ~UCSWRST;                   /*Initialize USCI state machine*/

    UCA0BR0 = 104;                          /*1MHz, 9600*/
    UCA0BR1 = 0;                            /*1MHz, 9600*/

    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
    UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
    UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;

    UCA0MCTL = UCBRS0;
    IE2 |= UCA0RXIE;
}

void RXdata_UART(unsigned char *c)
{
    while (!(IFG2 & UCA0RXIFG));              /*buffer Rx USCI_A0 plein ?*/
    *c = UCA0RXBUF;
}

void TXdata_UART( unsigned char c )
{
    while (!(IFG2 & UCA0TXIFG));              /*buffer Tx USCI_A0 vide ?*/
    UCA0TXBUF = c;
}

//cette fonction en interruption
void Send_STR_UART(unsigned char *msg)
{
    int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
    TXdata_UART(msg[i]);
    }
}

void menu_UART(void)
{

    switch(UCA0RXBUF)
    {
        case 'h' :
            IE2 |= UCA0TXIE;
            Send_STR_UART("ok\n");
            IE2 &=~ UCA0TXIE;
            break;

        case 'z' :
            avancer();
            break;

        case 's' :
            reculer();
            break;

        case 'd' :
            tourner_droite();
            break;

        case 'q' :
            tourner_gauche();
            break;

        case 'a' :
            arreter();
            break;

        default :
            Send_STR_UART("\rMauvaise commande ");
            /*Send_STR_UART(UCA0RXBUF);*/
            Send_STR_UART("\rEntrez 'h' pour l'aide\n");
            break;

    }
}

#ifndef ROBOT_SAMBOT_H
#define ROBOT_SAMBOT_H

void init_board(void);

void init_moteur(void);

void avancer(void);

void tourner_droite(void);

void tourner_gauche(void);

void arreter(void);

void reculer(void);

#endif

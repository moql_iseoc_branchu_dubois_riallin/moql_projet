#include <msp430g2553.h>
#include <string.h>

#include "robot_sambot.h"
#include "typedef.h"

/*Initialisation de l'UART*/
void init_UART(void)
{
    P1SEL |= (BIT1 + BIT2);                 /*P1.1 = RXD, P1.2=TXD*/
    P1SEL2 |= (BIT1 + BIT2);                /*P1.1 = RXD, P1.2=TXD*/

    UCA0CTL1 |= UCSSEL_2;                   /*SMCLK*/
    UCA0CTL1 &= ~UCSWRST;                   /*Initialize USCI state machine*/

    UCA0BR0 = 104;                          /*1MHz, 9600*/
    UCA0BR1 = 0;                            /*1MHz, 9600*/

    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
    UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
    UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;

    UCA0MCTL = UCBRS0;
    IE2 |= UCA0RXIE;
}

/*r�ception d'un caract�re en UART*/
void RXdata_UART(UCHAR *c)
{
    while ((IFG2 & UCA0RXIFG)==0){}              /*buffer Rx USCI_A0 plein ?*/
    *c = UCA0RXBUF;
}

/*transmission d'un caract�re en UART*/
void TXdata_UART( UCHAR c )
{
    while ((IFG2 & UCA0TXIFG)==0){}              /*buffer Tx USCI_A0 vide ?*/
    UCA0TXBUF = c;
}

/*Envoi d'un message en UART*/
void Send_STR_UART(UCHAR *msg)
{
    UINT32 i = 0U;
    for(i=0U ; msg[i] != 0x00U ; i++)
    {
    TXdata_UART(msg[i]);
    }
}

/*Menu qui d�termine ce que le robot doit faire en fonction des commandes re�ues en UART*/
void menu_UART(const UCHAR c)
{
    /*while (!(IFG2&UCA0RXIFG)); � utiliser ?*/

    switch(c)
    {
        case 'h' :
            Send_STR_UART("\r\nAide : commandes existantes :\n");
            Send_STR_UART("\r\n'z' : avancer\n");
            Send_STR_UART("\r\n'd' : tourner a droite\n");
            Send_STR_UART("\r\n'q' : tourner a gauche\n");
            Send_STR_UART("\r\n's' : reculer\n");
            Send_STR_UART("\r\n'a' : arret\n");
            Send_STR_UART("\r\n'h' : affichage de cette aide\n");
            break;

        case 'z' :
            avancer();
            Send_STR_UART("\rAvancer\n");
            break;

        case 's' :
            reculer();
            Send_STR_UART("\rReculer\n");
            break;

        case 'd' :
            tourner_droite();
            Send_STR_UART("\rDroite\n");
            break;

        case 'q' :
            tourner_gauche();
            Send_STR_UART("\rGauche\n");
            break;

        case 'a' :
            arreter();
            Send_STR_UART("\rArret\n");
            break;

        default :
            Send_STR_UART("\rMauvaise commande\n");
            Send_STR_UART(&c);
            Send_STR_UART("\n\rEntrez 'h' pour l'aide\n");
            break;

    }
}

#include <msp430g2553.h>
#include <string.h>

#include "robot_sambot.h"

void init_UART(void)
{
    P1SEL |= (BIT1 + BIT2);                 /*P1.1 = RXD, P1.2=TXD*/
    P1SEL2 |= (BIT1 + BIT2);                /*P1.1 = RXD, P1.2=TXD*/

    UCA0CTL1 |= UCSSEL_2;                   /*SMCLK*/
    UCA0CTL1 &= ~UCSWRST;                   /*Initialize USCI state machine*/

    UCA0BR0 = 104;                          /*1MHz, 9600*/
    UCA0BR1 = 0;                            /*1MHz, 9600*/

    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
    UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
    UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;

    UCA0MCTL = UCBRS0;
    IE2 |= (UCA0RXIE);
}

void RXdata_UART(unsigned char *c)
{
    while (!(IFG2 & UCA0RXIFG));              /*buffer Rx USCI_A0 plein ?*/
    *c = UCA0RXBUF;
}

void TXdata_UART( unsigned char c )
{
    while (!(IFG2 & UCA0TXIFG));              /*buffer Tx USCI_A0 vide ?*/
    UCA0TXBUF = c;
}

//cette fonction en interruption
void Send_STR_UART(unsigned char *msg)
{
    int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
    TXdata_UART(msg[i]);
    }
}

void menu_UART(unsigned char c)
{
    //while (!(IFG2&UCA0RXIFG));

    switch(c)
    {
        case 'h' :
            Send_STR_UART("\r\nAide : commandes existantes :\n");
            Send_STR_UART("\r\n'z' : avancer\n");
            Send_STR_UART("\r\n'd' : tourner a droite\n");
            Send_STR_UART("\r\n'q' : tourner a  gauche\n");
            Send_STR_UART("\r\n's' : reculer\n");
            Send_STR_UART("\r\n'a' : arret\n");
            Send_STR_UART("\r\n'h' : affichage de cette aide\n");
            break;

        case 'z' :
            avancer();
            Send_STR_UART("\rAvancer\n");
            break;

        case 's' :
            reculer();
            Send_STR_UART("\rReculer\n");
            break;

        case 'd' :
            tourner_droite();
            Send_STR_UART("\rDroite\n");
            break;

        case 'q' :
            tourner_gauche();
            Send_STR_UART("\rGauche\n");
            break;

        case 'a' :
            arreter();
            Send_STR_UART("\rArret\n");
            break;

        default :
            Send_STR_UART("\rMauvaise commande\n");
            Send_STR_UART(&c);
            Send_STR_UART("\n\rEntrez 'h' pour l'aide\n");
            break;

    }
}

#include <msp430g2553.h>

int flag = 0; /*0=arret; 1=avancer; -1=reculer*/

void init_bouton(void)
{
    //bouton demarrage
    P1DIR &=~ BIT3;
    P1REN |= BIT3;
    P1OUT |= BIT3;
    P1IE |= BIT3;
    P1IES |= BIT3;
    P1IFG &=~ BIT3;
}

void init_moteur(void)
{
    P2DIR |= (BIT2 | BIT4 | BIT1 | BIT5);

    P2OUT |= BIT5; /*sens moteur B*/
    P2OUT &=~ (BIT1 | BIT2 | BIT4); /*sens moteur A*/
}

void arreter()
{
    P2OUT &=~ (BIT5 | BIT1 | BIT2 |BIT4);
    flag = 0;
}

void avancer()
{
    P2OUT &=~ BIT1;
    P2OUT |= (BIT5 | BIT2 | BIT4);
    flag = 1;
}


void reculer()
{
    P2OUT |= (BIT1 | BIT2 | BIT4);
    P2OUT &=~ BIT5;
    flag = -1;
}


void tourner_gauche()
{
    P2OUT |= (BIT1 | BIT2 | BIT4 | BIT5);
    __delay_cycles(100000);

    if (flag == 1)
    {
        avancer();
    }
    else if (flag == -1)
    {
        reculer();
    }
    else
    {
        arreter();
    }

}


void tourner_droite()
{
    P2OUT |= (BIT2 | BIT4);
    P2OUT &=~ (BIT5 | BIT1);
    __delay_cycles(100000);

    if (flag == 1)
    {
        avancer();
    }
    else if (flag == -1)
    {
        reculer();
    }
    else
    {
        arreter();
    }
}

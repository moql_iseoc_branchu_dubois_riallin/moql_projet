#include <msp430g2553.h>
#include <string.h>

#include "robot_sambot.h"

void init_UART(void)
{
    P1SEL |= (BIT1 + BIT2);                 /*P1.1 = RXD, P1.2=TXD*/
    P1SEL2 |= (BIT1 + BIT2);                /*P1.1 = RXD, P1.2=TXD*/

    UCA0CTL1 |= UCSSEL_2;                   /*SMCLK*/
    UCA0CTL1 &= ~UCSWRST;                   /*Initialize USCI state machine*/

    UCA0BR0 = 104;                          /*1MHz, 9600*/
    UCA0BR1 = 0;                            /*1MHz, 9600*/

    UCA0CTL0 &= ~UCPEN & ~UCPAR & ~UCMSB;
    UCA0CTL0 &= ~UC7BIT & ~UCSPB & ~UCMODE1;
    UCA0CTL0 &= ~UCMODE0 & ~UCSYNC;

    UCA0MCTL = UCBRS0;
    IE2 |= (UCA0RXIE | UCA0TXIE);
}

void RXdata_UART(unsigned char *c)
{
    while (!(IFG2 & UCA0RXIFG));              /*buffer Rx USCI_A0 plein ?*/
    *c = UCA0RXBUF;
}

void TXdata_UART( unsigned char c )
{
    while (!(IFG2 & UCA0TXIFG));              /*buffer Tx USCI_A0 vide ?*/
    UCA0TXBUF = c;
}

//cette fonction en interruption
void Send_STR_UART(unsigned char *msg)
{
    int i = 0;
    for(i=0 ; msg[i] != 0x00 ; i++)
    {
    TXdata_UART(msg[i]);
    }
}

void menu_UART(void)
{

    switch(UCA0RXBUF)
    {
        case 'h' :
            Send_STR_UART("\r\nAide : commandes existantes :\n");
            Send_STR_UART("\r\n'z' : avancer\n");
            Send_STR_UART("\r\n'd' : tourner a droite\n");
            Send_STR_UART("\r\n'q' : tourner a� gauche\n");
            Send_STR_UART("\r\n's' : reculer\n");
            Send_STR_UART("\r\n'a' : arret\n");
            Send_STR_UART("\r\n'h' : affichage de cette aide\n");
            break;

        case 'z' :
            avancer();
            break;

        case 's' :
            reculer();
            break;

        case 'd' :
            tourner_droite();
            break;

        case 'q' :
            tourner_gauche();
            break;

        case 'a' :
            arreter();
            break;

        default :
            Send_STR_UART("\rMauvaise commande ");
            /*Send_STR_UART(UCA0RXBUF);*/
            Send_STR_UART("\rEntrez 'h' pour l'aide\n");
            break;

    }
}

/*void command(const char *cmd )
{
  if(strcmp(cmd, "h") == 0)          // aide
  {
    	Send_STR_UART("\r\nAide : commandes existantes :\n");
    	Send_STR_UART("\r\n'z' : avancer\n");
    	Send_STR_UART("\r\n'd' : tourner a droite\n");
    	Send_STR_UART("\r\n'q' : tourner à gauche\n");
    	Send_STR_UART("\r\n's' : reculer\n");
    	Send_STR_UART("\r\n'x' : arret\n");
    	Send_STR_UART("\r\n'h' : affichage de cette aide\n");
  }
  else if (strcmp(cmd, "z") == 0)     // avancer
  {
	avancer();
    	Send_STR_UART("\rAvancer\n");
  }
  else if(strcmp(cmd, "d") == 0)     // tourner à droite
  {
	tourner_droite();
    	Send_STR_UART("\rTourner droite\n");
  }
  else if(strcmp(cmd, "q") == 0)     // tourner à gauche
  {
	tourner_gauche();
    	Send_STR_UART("\rTourner gauche\n");
  }
  else if(strcmp(cmd, "s") == 0)     // reculer
  {
	reculer();
    	Send_STR_UART("\rReculer\n");
  }
    else if(strcmp(cmd, "x") == 0)     // reculer
  {
	arreter();
    	Send_STR_UART("\rArret\n");
  }
  else
  {
    	Send_STR_UART("\rMauvaise commande ");
    	Send_STR_UART(cmd);
    	Send_STR_UART("\rEntrez 'h' pour l'aide\n");
  }
}*/

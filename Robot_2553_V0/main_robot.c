#include <msp430g2553.h>
#include <string.h>

#include "robot_sambot.h"
#include "UART_2553.h"

int i = 0;

void main(void)
{

    WDTCTL = WDTPW + WDTHOLD; /*Stop WDT*/
    /*clock calibration verification*/

    if(CALBC1_1MHZ==0xFF || CALDCO_1MHZ==0xFF)
      __low_power_mode_4();

    BCSCTL1 = CALBC1_1MHZ; /*Set DCO*/
    DCOCTL = CALDCO_1MHZ;

    init_UART();
    init_moteur();

    P1DIR |= BIT1;

    __bis_SR_register(LPM0_bits + GIE); /*Enter LPM0, interrupts enabled*/

    while(1){}

}


#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{
    while (!(IFG2&UCA0RXIFG));
    menu_UART();
}

/*#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
    while (!(IFG2&UCA0TXIFG));
    if(i==1){
        Send_STR_UART("\r\nAide : commandes existantes :\n");
        Send_STR_UART("\r\n'z' : avancer\n");
        Send_STR_UART("\r\n'd' : tourner a droite\n");
        Send_STR_UART("\r\n'q' : tourner a  gauche\n");
        Send_STR_UART("\r\n's' : reculer\n");
        Send_STR_UART("\r\n'a' : arret\n");
        Send_STR_UART("\r\n'h' : affichage de cette aide\n");
        P1OUT |= BIT0;
    }

}*/
